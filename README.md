# Pantheon Decoupled

Recommended configuration for Decoupled sites on [Pantheon](https://pantheon.io/). Can be used in combination with Pantheon's front end starter kits including:

[https://github.com/pantheon-systems/next-drupal-starter](https://github.com/pantheon-systems/next-drupal-starter)

## pantheon_decoupled

- Installs recommended dependencies for a Decoupled Drupal backend including:
  - [pantheon-advanced-page-cache](https://www.drupal.org/project/pantheon_advanced_page_cache) for improved caching and cache purging for decoupled use cases on the Pantheon platform.
  - [jsonapi_menu_items](https://www.drupal.org/project/jsonapi_menu_items) and [jsonapi_hypermedia](https://www.drupal.org/project/jsonapi_hypermedia) to support common JSON:API use cases.
  - [build_hooks](https://www.drupal.org/project/build_hooks) to trigger front-end webhooks on Drupal events.
  - [anonymous_login](https://www.drupal.org/project/anonymous_login) to limit anonymous access to Drupal rendered content.
  - Optional [decoupled_kit_pantheon_search](https://www.drupal.org/project/decoupled_kit_pantheon_search) which can be enabled to provide a reference implementation for decoupled search.
- Configures an example preview site and oAuth consumer for use with Decoupled Preview.
- Adds a 'Pantheon Front-End Sites' configuration page under 'Web Services' in the admin menu. This page allows you manage and test your front-end preview sites, and also provides in context links to useful documentation.

### Note
- The `minimum-stability` should be set to `alpha` in the project's root
 `composer.json` file.

## pantheon_decoupled_example

`pantheon_decoupled` also includes a sub-module named
`pantheon_decoupled_example`. This module adds limited content integrations.
Enabling this module will also enable the media and media_library modules.
This will also add media fields (articles only) and enables pathauto for the
article and basic page content types.

## Installation

Require using composer:

```
composer require drupal/pantheon_decoupled
```

Enable pantheon_decoupled and pantheon_decoupled_example using your preferred method.