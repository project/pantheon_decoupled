<?php

namespace Drupal\pantheon_decoupled\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class EnvVarController extends ControllerBase {

  protected $formBuilder;

  public function __construct(FormBuilderInterface $formBuilder) {
    $this->formBuilder = $formBuilder;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('form_builder')
    );
  }

  public function EnvVarRegenerateForm() {
    $form = $this->formBuilder->getForm('\Drupal\pantheon_decoupled\Form\EnvVarForm');
    return $form;
  }
}
