<?php

namespace Drupal\pantheon_decoupled\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormStateInterface;


/**
 * Controller for FES Settings Page.
 */
class FesController extends ControllerBase {

  public function content() {
    $list_builder = \Drupal::entityTypeManager()->getListBuilder('dp_preview_site');
    $rendered_list = $list_builder->render();
    return [
      'intro' => ['#theme' => 'pantheon_decoupled_fes_settings'],
      'preview_list_header' => [
        'header_prefix' => [
          '#markup' => '<div class="fes-preview-list">',
        ],
        'preview_list_heading' => [
          '#prefix' => '<h2>',
          '#markup' => t('Preview Site configuration'),
          '#suffix' => '</h2>',
        ],
        'add_preview_site' => [
          '#prefix' => '<a href="/admin/config/services/pantheon-fes/add" class="button button--action button--primary button--fes-add" data-drupal-link-system-path="/admin/config/services/pantheon-fes/add">',
          '#markup' => t('Add Preview Site'),
          '#suffix' => '</a>'
        ],
        'header_suffix' => [
          '#markup' => '</div>',
        ],
      ],
      'list' => $rendered_list,
    ];
  }
}