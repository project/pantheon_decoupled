<?php

namespace Drupal\pantheon_decoupled\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Routing\RouteMatchInterface;


class EnvVarForm extends FormBase
{
    protected $routeMatch;

    public function __construct(RouteMatchInterface $routeMatch)
    {
        $this->routeMatch = $routeMatch;
    }

    public static function create(ContainerInterface $container)
    {
        return new static(
            $container->get('current_route_match')
        );
    }
    public function getFormId()
    {
        return 'pantheon_decoupled_regenerate_env_vars';
    }

    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $preview_site = $this->routeMatch->getParameter('preview_site');
        $storage = \Drupal::entityTypeManager()->getStorage('dp_preview_site');
        $sites = $storage->loadMultiple();
        $site = $storage->load($preview_site);
        $client_id = $site->get('oauth_consumer');
        $preview_secret = $site->get('secret');
        $url = \Drupal::request()->getSchemeAndHttpHost();

        // Get associated consumer
        $consumer_storage = \Drupal::entityTypeManager()->getStorage('consumer');
        $consumers = $consumer_storage->loadByProperties(['client_id' => $client_id]);
        $consumer = reset($consumers);
        $form_state->set('consumer', $consumer);


        $form = [
            'preview_secret' => [
                '#prefix' => '<p><b>',
                '#markup' => t('PREVIEW_SECRET:'),
                '#suffix' => '</b></p>',
            ],
            'preview_secret_value' => [
                '#prefix' => '<p>',
                '#markup' => t($preview_secret),
                '#suffix' => '</p>',
            ],
            'client_id' => [
                '#prefix' => '<p><b>',
                '#markup' => t('CLIENT_ID:'),
                '#suffix' => '</b></p>',
            ],
            'client_id_value' => [
                '#prefix' => '<p>',
                '#markup' => t($client_id),
                '#suffix' => '</p>',
            ],
            'client_secret' => [
                '#prefix' => '<p><b>',
                '#markup' => t('CLIENT_SECRET:'),
                '#suffix' => '</b></p>',
            ],
            'client_secret_value' => [
                '#prefix' => '<p>',
                '#markup' => t('This module does not store your client secret key. If needed, this variable can be regenerated.'),
                '#suffix' => '</p>',
            ],
            'client_wrapper' => [
                '#type' => 'container',
                '#attributes' => ['id' => 'client_wrapper'],
            ],
            'client_secret_regenerate' => [
                '#type' => 'submit',
                '#value' => $this->t('Generate random Client Secret'),
                '#attributes' => [
                    'class' => [
                        'button--small',
                    ],
                ],
                '#ajax' => [
                    'callback' => [$this, 'generateClientSecret'],
                    'disable-refocus' => TRUE,
                    'wrapper' => 'client_wrapper',
                ],
            ],
            'linked_cms' => [
                '#prefix' => '<p><b>',
                '#markup' => t('Linked CMS:'),
                '#suffix' => '</b></p>',
            ],
            'linked_cms_value' => [
                '#prefix' => '<p>',
                '#markup' => 'Link the CMS site that relates to: ' . '<b>' . $url . '</b>',
                '#suffix' => '</p>',
            ],
        ];

        return $form;
    }

    /**
     * This method is required by FormBase
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        // Handle form submission.
    }

    public function generateClientSecret(array &$form, FormStateInterface $form_state): array
    {
        $consumer = $form_state->get('consumer');
        $client_secret = \Drupal::service('password_generator')->generate(16);
        $consumer->set('secret', $client_secret);
        $consumer->save();

        $form['client_wrapper']['updated_field'] = [
            '#type' => 'textfield',
            '#title' => $this->t('<b>New CLIENT_SECRET</b>'),
            '#value' => $client_secret,
        ];

        return $form['client_wrapper'];
    }
}
